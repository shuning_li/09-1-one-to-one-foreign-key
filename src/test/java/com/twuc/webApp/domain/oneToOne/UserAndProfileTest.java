package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        UserEntity user = new UserEntity();
        UserProfileEntity userProfile = new UserProfileEntity("Li", "Shuning");
        user.setUserProfileEntity(userProfile);

        ClosureValue<Long> userIdValue = new ClosureValue<>();
        ClosureValue<Long> userProfileIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            userEntityRepository.save(user);
            userIdValue.setValue(user.getId());
            userProfileIdValue.setValue(userProfile.getId());
        });

        run(em -> {
            UserEntity queryUser = userEntityRepository.getOne(userIdValue.getValue());
            UserProfileEntity queryUserProfile = userProfileEntityRepository.getOne(userProfileIdValue.getValue());

            assertNotNull(queryUser);
            assertNotNull(queryUserProfile);
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        UserEntity user = new UserEntity();
        UserProfileEntity userProfile = new UserProfileEntity("Li", "Shuning");
        user.setUserProfileEntity(userProfile);

        ClosureValue<Long> userIdValue = new ClosureValue<>();
        ClosureValue<Long> userProfileIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            userEntityRepository.save(user);
            userIdValue.setValue(user.getId());
            userProfileIdValue.setValue(userProfile.getId());
        });

        flushAndClear(em -> {
            userEntityRepository.delete(user);
        });

        run(em -> {
            Optional<UserEntity> optionalUser = userEntityRepository.findById(userIdValue.getValue());
            Optional<UserProfileEntity> optionalUserProfile = userProfileEntityRepository.findById(userProfileIdValue.getValue());

            assertFalse(optionalUser.isPresent());
            assertFalse(optionalUserProfile.isPresent());
        });
        // --end->
    }
}